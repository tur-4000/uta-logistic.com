$(document).ready(function() {

/*----------------------------------------------------*/
/*	Sequence Slider
/*----------------------------------------------------*/
     
    $(function(){
        var options = {
            nextButton: true,
            prevButton: true,
            pagination: true,
            animateStartingFrameIn: true,
            autoPlay: true,
            autoPlayDelay: 3000,
            preloader: true,
            preloadTheseFrames: [1],
        };

        var mySequence = $("#sequence").sequence(options).data("sequence");
    });

/*----------------------------------------------------*/
/*	Flexisel Clientslider
/*----------------------------------------------------*/

    $("#clientslider").flexisel({
            visibleItems: 6,
            animationSpeed: 1000,
            autoPlay: true,
            autoPlaySpeed: 6000,
            pauseOnHover: true,
            enableResponsiveBreakpoints: true,
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 1
                },
                landscape: {
                    changePoint:640,
                    visibleItems: 2
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 3
                }
            }
    });

/*----------------------------------------------------*/
/*	Sticky Nav
/*----------------------------------------------------*/

    $(window).load(function(){
          $("#menu").sticky({ topSpacing: 0 });
    });

/*----------------------------------------------------*/
/*	About Us BxSlider
/*----------------------------------------------------*/

    $(function(){
                 $('.aboutus').bxSlider({
                     mode: 'horizontal',
                     slideMargin: 3,
                     auto:true
                 });
    });


/*----------------------------------------------------*/
/*	Testemonials BxSlider
/*----------------------------------------------------*/

    $(function(){
                 $('.testemonials').bxSlider({
                     mode: 'horizontal',
                     slideMargin: 3,
                     auto:true
                 });
    });

});