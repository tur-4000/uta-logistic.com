<?php
require_once(__DIR__ . '/../app/stat.php');
?>
<!DOCTYPE html>

<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title>УТА Логистик</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="УТА Логистик - перевозка различных видов зерновых и масличных грузов, а также продуктов их переработки собственным и наёмным автотранспортом">
    <meta name="author" content="Valeriy Turbanov based on ThreeSixty Template by Fabian Bentz Webdesign">

    <!-- google web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">

    <!-- CSS Styles -->
    
    <!-- bootstrap css -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    
    <!-- standart theme css -->
    <link rel="stylesheet" id="theme" href="./css/theme1.css" type="text/css" />
    
    <!-- Font Awesome Icons css -->
    <link rel="stylesheet" href="./font-awesome/css/font-awesome.min.css">
    
    <!-- BxSlider and Sequence Slider css -->
    <link rel="stylesheet" href="./css/jquery.bxslider.css" type="text/css" />
    <link rel="stylesheet" media="screen" href="./css/sequence-slider.css" />
    
    <!-- Back to Top Button css -->
    <link rel="stylesheet" href="./css/top.css" type="text/css" />

    <!-- custom css -->
    <link href="./css/custom.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <!--<script src="../js/html5shiv.js"></script>-->
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="./img/favicon.png">
    <link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./img/apple-touch-icon-114x114.png">
	
	
	<!-- Your Google Analytics Code Here!! -->

	
  </head>

<body>
  
<!-- START PRIMARY LAYOUT
======================== -->

<!-- Back to Top Button (visible after scrolling 1200px down) Change Styles at the top.css file -->

<div class="button-top" id="top-bt">

  <a href="#about"><i class="fa fa-sort-desc fa-3x"></i></a>
  
</div>

<!-- Back to Top Button END -->

<!-- NAVBAR -->
    
<!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->

<nav class="navbar navbar-inverse navbar-static-top" role="navigation" id="menu">
          <div class="container container-nav">
            <div class="navbar-header">
              <a class="navbar-brand nav-logo" href="#">
                <img alt="Brand" src="img/logo/UTA-logistic-ru.png" height="40px">
              </a>
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
           <!-- MENU LINKS -->
           <div class="navbar-collapse collapse navbar-right">
              <ul class="nav navbar-nav" id="navigation">
                <li><a href="#about">О нас</a></li>
                <li><a href="#avto">Наш автопарк</a></li>
                <li><a href="#service">Наши услуги</a></li>
                <li><a href="#clients">Наши партнёры</a></li>
                <li><a href="#map">Контакты</a></li>
              </ul>
          </div>
        </div>
</nav>

<!-- NAVBAR END -->
  
<!-- HEADER -->

<header class="container-fluide hidden-xs">
  <div class="header row">
    <div class="col-12">
      <div class="logo-head">
        <h1>УТА ЛОГИСТИК</h1>
        <h3><i>ПУТЬ, КОТОРЫЙ ИСКАЛИ ВЫ</i></h3>
      </div>
    </div>
  </div>
</header>

<!-- HEADER END -->


<!-- ABOUT US -->
      
<div id="about">

 <div class="container">
    
    <div class="center-block head">
    
       <h1>О нас</h1>
       <hr class="head-border-grey">
       
    </div>
      
        <div class="agency col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
         <p>ООО «УТА Логистик» является современной специализированной логистической компанией, основным направлением деятельности которой является перевозка различных видов зерновых и масличных грузов, а также продуктов их переработки собственным и наёмным автотранспортом.</p>

         <p>Компания ООО «УТА Логистик» была создана в начале 2015 года на производственной базе ООО «Укртрансагро», в связи с необходимостью предоставления клиентам комплексных логистических услуг высокого качества, которые бы соответствовали лучшим мировым стандартам.</p>
        
       </div>
       
<!-- About Us Slider. Change Styles at the jquery.bxlsider.css file -->

      <div class="col-lg-5 col-md-5">
        <ul class="aboutus">
          <li>
            <img class="img-responsive" src="./img/terminal-1.jpg" alt="Scania на терминале">
          </li>
          <li>
            <img class="img-responsive" src="./img/terminal-2.jpg" alt="Scania на терминале">
          </li>
          <li>
            <img class="img-responsive" src="./img/terminal-3.jpg" alt="Scania на терминале">
          </li>
          <li>
            <img class="img-responsive" src="./img/terminal-4.jpg" alt="Scania на терминале">
          </li>
        </ul>
      </div>
         
 </div>
</div>

<!-- ABOUT END -->

<!-- НАШ АВТОПАРК -->

<div id="avto">

  <div class="container">

    <div class="center-block head">

      <h1>Наш автопарк</h1>
      <hr class="head-border-grey">

    </div>

    <div class="agency col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
      <ul class="aboutus">
        
        <li>
          <img class="img-responsive" src="./img/scania-3.jpg" alt="YOUR TEXT HERE">
        </li>
    	  
        <li>
          <img class="img-responsive" src="./img/scania-2.jpg" alt="YOUR TEXT HERE">
        </li>
         
        <li>
          <img class="img-responsive" src="./img/scania-1.jpg" alt="YOUR TEXT HERE">
        </li>

      </ul>
    </div>

    <div class="col-lg-5 col-md-5">
      <p>На данный момент автопарк компании состоит из 5 автомобилей Scania модели G420 (седельные тягачи).</p>
      <p>Двухосные тягачи с одним ведущим мостом (4&times;2) и главной передачей R780, 12-ступенчатой коробкой передач.</p>
      <p>На всех автомобилях установлены системы навигации GPS, что позволяет информировать клиентов о ходе перевозки в режиме on-line.</p>
      <p>Полуприцепы-зерновозы Schmitz, стальные трехосные самосвальные прицепы-зерновозы объемом 52м<sup>3</sup>, с осями производства SAF и разгрузкой назад.</p>
    </div>

  </div>
</div>

<!-- END НАШ АВТОПАРК -->
     
<!-- SERVICE -->
     
<div id="service">

  <div class="container">

    <div class="row">
    
     <div class="center-block head">
       
       <div class="col-lg-12 col-md-12 col-xs-12">
     
        <h1>Что мы предоставляем</h1>
        <hr class="head-border-white">
       </div>
          
      </div>
      
        <!-- SERVICE COLUMNS -->   
        <div class="col-lg-4 col-md-4 service">
         <div class="service-col">
         
           <div class="icon-desktop rotate">
             <img src="./img/icons/rate_1.png" alt="">
           </div>
           
           <h4>Выгодные <nobr>тарифы</nobr></h4>
          <p>Подбор оптимального маршрута и согласование идеального графика вывоза. Формирование  наиболее выгодной ставки.</p>
          
         </div>
        </div>
        
        <div class="col-lg-4 col-md-4 service">
        <div class="service-col">
        
          <div class="icon-wrench rotate">
            <img src="./img/icons/control_1.png" alt="">
          </div>
           
          <h4>Контроль<br>on-line</h4>
          <p>Предоставление возможности контроля и корректировки рейса в режиме on-line, благодаря современной системе слежения за автопарком.</p>
          
          </div>
        </div>
        
        <div class="col-lg-4 col-md-4 service">
        <div class="service-col">
        
          <div class="icon-mobile rotate">
            <img src="./img/icons/reliability_1.png" alt="">
           </div>
           
          <h4>Сохранность груза</h4>
          <p>Минимальные потери при перевозке, а также максимально быстрая и качественная выгрузка.</p>
          
        </div>
      </div>
      
     </div>
   </div>
</div>

<!-- PROCESS END -->

<!-- CLIENTS -->

<div id="clients">

  <div class="container">
      <div class="row">
    
      <div class="center-block head">
      
       <div class="col-lg-12 col-md-12 col-xs-12">
     
        <h1>Наши партнеры</h1>
        <hr class="head-border-grey">
         <p class="d-grey center">МЫ обеспечиваем весь цикл логистики в агросекторе, оптимизируя процессы на каждом этапе, от производителя до конечного покупателя</p>
        
       </div>
         
      </div>
      
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
    
      <ul class="testemonials">
            
			  <li>
			  <blockquote>
				<p><i>Закупка и экспорт зерновых культур непосредственно от производителя</i></p>
				<p class="author">«Свит Агро Трейд»</p>
			  </blockquote>
			  </li>
			  
		      <li>
		      <blockquote>
		       <p><i>Зерноперевалочный комплекс в акватории Азовского моря</i></p>
		       <p class="author">«УкрТрансАгро»</p>
		     </blockquote>
		     </li>
		     
		    <li>
          <blockquote>
            <p><i>Азовский судоремонтный завод (ООО «СРЗ»)  специализацией которого является судостроение, судоремонт и перевалка грузов.</i></p>
            <p class="author">АСРЗ</p>
          </blockquote>
        </li>

        <li>
          <blockquote>
            <p><i>Карантинное обеззараживание грузов перед экспортом</i></p>
            <p class="author">«Мартлет»</p>
          </blockquote>
        </li>

        <li>
          <blockquote>
            <p><i>Агентирование судов, комплексное управление флотом</i></p>
            <p class="author">«Маритайм Логистикс»</p>
          </blockquote>
        </li>

        <li>
          <blockquote>
            <p><i>Управляющая компания, занимающаяся стратегическим развитием компаний в сфере аграрной и  морской логистики, машиностроения и медиабизнесе.</i></p>
            <p class="author">УК «МИГ»</p>
          </blockquote>
        </li>
		
		  </ul>
    
    </div>
           
<!-- CLIENTS SLIDER -->

    <div class="col-lg-12 col-md-12">
      <ul id="clientslider">
        <li><a href="https://www.ukmig.com.ua"><img src="./img/logo/mig.png" alt="MiG"/></a></li>
        <li><a href="https://ukrtransagro.com/"><img src="./img/logo/uta.png" alt="UTA"/></a></li>
        <li><a href="https://maritime-logistics.com.ua/"><img src="./img/logo/mtl.png" alt="MTL"/></a></li>
        <li><a href="https://martlet.com.ua/"><img src="./img/logo/martlet.png" alt="Martlet"/></a></li>
        <li><a href="https://svitagrotrade.com/"><img src="./img/logo/SAT_logo.png" alt="SAT"/></a></li>
        <li><a href="http://asrz.com.ua/"><img src="./img/logo/SRZ-logo.png" alt="SRZ"/></a></li>
        <!-- ADD MORE IMAGE HERE -->
      </ul>
    </div>

    </div>
  </div>
</div>

<!-- CLIENTS END -->

<div class="col-lg-12">
  <div class="up"><a href="#about"><span class="glyphicon glyphicon-chevron-up"></span></a></div>
</div>

<!-- FACTS END -->

<!-- GOOGLE MAP DIVIDER -->

<div id="map">
  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2717.5032208596654!2d37.51079174427876!3d47.06959458995244!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40e6e4b9a9a88503%3A0x5ffc8c351dd455f0!2z0L_RgNC-0YHQvy4g0JvRg9C90LjQvdCwLCAxMywg0JzQsNGA0LjRg9C_0L7Qu9GMLCDQlNC-0L3QtdGG0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA4NzUwMA!5e0!3m2!1sru!2sua!4v1555318705550!5m2!1sru!2sua" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<!-- GOOGLE MAP DIVIDER END -->

<!-- BOTTOM -->

<div id="foot">

  <div class="container" id="contact">
    <div class="row justify-content-around">
     
      <div class="col-md-3 col-md-offset-3">
        <div class="bottom-con">
        
           <h5>Адрес</h5>
           <hr class="head-border-white">
            
             <div class="">
             <p><i class="fa fa-location-arrow"><span>  пр. Лунина, 13</span></i></p>
             <p><i class="fa fa-map-marker"><span> 87510 г. Мариуполь</span></i></p>
             <p><i class="fa fa-flag"><span> Украина</span></i></p>
           </div>
            
        </div>
      </div>
 
      <div class="col-md-3">
        <div class="bottom-con">
        
          <h5>Контакты</h5>
          <hr class="head-border-white">

          <div class="">
            <p><i class="fa fa-phone"></i> <span>+380 629 40-90-17</span></p>
            <p>
              <i class="fa fa-envelope"></i>
              <span><a href="mailto:info@uta-logistic.com">info@uta-logistic.com</a></span>
            </p>
            <p>
              <i class="fa fa-facebook-square"></i>
              <span><a href="https://www.facebook.com/utalogisticmariupol/">УТА Логистик</a></span>
            </p>
          </div>
            
        </div>
      </div>

    </div>

    <div class="row">
      <div class="col-lg-12 col-md-12 footer">
        <p class="pull-right text-info copy-text"><a href="#about">&copy; УТА Логистик</a></p>
      </div>
    </div>

  </div>
  
</div>

<!-- FOOTER END -->

<!-- END PRIMARY LAYOUT
====================== -->


<!-- JAVASCRIPT
================================================== -->

<!-- BOOTSTRAP JAVASCRIPT -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="./js/bootstrap.min.js"></script>

<!-- CUSTOM JAVASCRIPT -->
<!-- Custom Functions -->
<script type="text/javascript" src="./js/functions.js"></script>

<!-- BxSlider -->
<script type="text/javascript" src="./js/jquery.bxslider.min.js"></script>

<!-- Sequence Slider -->
<script type="text/javascript" src="./js/jquery.sequence-min.js"></script>

<!-- Parallax Background -->
<script type="text/javascript" src="./js/nbw-parallax.js"></script>
<script type="text/javascript" src="./js/jquery.inview.js"></script>

<!-- Smooth Scrolling -->
<script type="text/javascript" src="./js/smoothscroll.js"></script>

<!-- Sticky Navigation --> 
<script type="text/javascript" src="./js/jquery.sticky.js"></script>

<!-- Clients Slider -->
<script type="text/javascript" src="./js/jquery.flexisel.js"></script>

<!-- Retina JS -->
<script type="text/javascript" src="./js/retina-1.1.0.min.js"></script>


</body>
</html>